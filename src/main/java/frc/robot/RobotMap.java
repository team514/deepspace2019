/*----------------------------------------------------------------------------*/
/* Copyright (c) 2017-2018 FIRST. All Rights Reserved.                        */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot;

/**
 * The RobotMap is a mapping from the ports sensors and actuators are wired into
 * to a variable name. This provides flexibility changing wiring, makes checking
 * the wiring easier and significantly reduces the number of magic numbers
 * floating around.
 */
public class RobotMap {
  /**
   * Assist Mode Variables
   */
  public static final double driveMecanumXBanner = 0.5;
  public static final double driveMecanumXVision = 0.4; //used to be 0.65
  public static final double driveMecanumY = 0.30;
  public static final double driveMecanumZ = 0.3;
  
  public static final double targetAreaSize = 30.0;
  public static final double targetHorizontal = 0.0;
  
  public static final double deployHatchBackupDistance = 48.0;

  public static final double S2W_TurnSpeed = 0.45;
  public static final double S2W_SonarDistance = 27.5;

  public static final double shotDistance = 18.0;
  public static final double upperDefaultShotSpeed = 0.5;
  public static final double lowerDefaultShotSpeed = 0.5;
  public static final double upperHighShotSpeed = 1.0;
  public static final double upperIntakeStopSpeed = 0.2;

  public static final double upperCargoShipShot = 0.3;
  public static final double lowerCargoShipShot = 0.55;

  public static final double lowerRocketLowShot = 0.2;
  public static final double upperRocketLowShot = 0.3;
  public static final double lowerRocketMidShot = 0.45;   //TODO: I think the Lower and Upper Motor values should be reversed!
  public static final double upperRocketMidShot = 0.675;
  public static final double lowerRocketHighShot = 1.0;   //TODO: I think Lower should be 0.8 and the Upper should be 0.6 
  public static final double upperRocketHighShot = 0.9;

  public static final double deployCargoRocketHigh = 30.0;
  public static final double deployCargoRocketMid = 14.0;
  public static final double deployCargoShip = 18.0;
  public static final double encoderDriveSpeed = 0.25; //TODO: Change to 0.3, adjusted to .2 for floor testing

  public static final double cargoIn = 0.2;
  public static final double cargoOut = -0.2;

  /**
   * DriveUtil Variables
   */
  public static final int leftFrontSC = 0;
  public static final int leftRearSC = 1;
  public static final int rightFrontSC = 2;
  public static final int rightRearSC = 3;

  public static final int driveEncoderFoward = 6;
  public static final int driveEncoderReverse = 7;

  public static final int sonarCargoLeft = 0;
  public static final int sonarCargoRight = 1;
  public static final int sonarHatchLeft = 2;
  public static final int sonarHatchRight = 3;
  public static final int cargoAverageBits = 2;
  public static final int cargoOversampleBits = 4;
  public static final int hatchAverageBits = 2;
  public static final int hatchOversampleBits = 4;

  public static final int leftOuter = 2;
  public static final int leftInner = 0;
  public static final int center = 4;
  public static final int rightInner = 1;
  public static final int rightOuter = 3;

  public static final int sonarCargoSequenceOutput = 6;
  public static final int sonarHatchSequenceOutput = 7;
  public static final double sonarRange = 84.0;

  public static final double zAdjust = 0.75;

  public static final int ticksPerInch = 7; //6.79



   /**
    * Vision Util Variables 
    */ 
  public static final int msServo = 0;
  public static final double camServoMax = 180.0;
  public static final double camServoMin = 0.0;
  public static final double centerWindowDegrees = 3.0;
  public static final double camServoDefaultAngle = 0.0;

  /**
   * Coerce to Range Values
   */
  public static final double C2RM_inputMin = -27.0;
  public static final double C2RM_inputMax = 27.0;
  public static final double C2RM_outputMin = -0.5;
  public static final double C2RM_outputMax = 0.5;  
  public static final double C2R_servoMin = 0.0;
  public static final double C2R_servoMax = 20.0;
  public static final double C2RMA_outputMin = 0.0; //dummy
  public static final double C2RMA_outputMax = 10.0; //dummy
  public static final double C2RMA_inputMin = -27.0; //dummy
  public static final double C2RMA_inputMax = 27.0; //dummy
  
  /**
   * Cargo Util Variables
   */
  public static final int lowerShotMotor = 4;
  public static final int upperShotMotor = 5;
  public static final int cargoSwitch = 5;

  /**
   * Hatch Util Variables
   */
  public static final int panelLatch = 0;
  public static final int hatchSwivel = 1;
  public static final int hatchSwitch = 8;

  /**
   * Joystsick Mapping
   */
    public static final int kJoystickButton1 = 1;
    public static final int kJoystickButton2 = 2;
    public static final int kJoystickButton3 = 3;
    public static final int kJoystickButton4 = 4;
    public static final int kJoystickButton5 = 5;
    public static final int kJoystickButton6 = 6;
    public static final int kJoystickButton7 = 7;
    public static final int kJoystickButton8 = 8;
    public static final int kJoystickButton9 = 9;
    public static final int kJoystickButton10 = 10;
    public static final int kJoystickButton11 = 11;
    public static final int kJoystickButton12 = 12;
  /**
   * Controler Mapping
   */
  
    //Controller Button Map goes here...
    //LogiTech F310 Button Mapping X/D Switch = D, Direct Input
    //Axis
		public static final int kLeftXAxisNum = 0;
		public static final int kLeftYAxisNum = 1;
		public static final int kRightXAxisNum = 2;
		public static final int kRightYAxisNum = 3;
		
		//For DPad, use controller.getPOV();
		//public static final int kDPadXAxisNum = 5;
		//public static final int kDPadYAxisNum = 6;
		
		public static final int kXButtonNum = 1;
		public static final int kAButtonNum = 2;
		public static final int kBButtonNum = 3;
		public static final int kYButtonNum = 4;
		public static final int kLeftBumperNum = 5;
		public static final int kRightBumperNum = 6;
		public static final int kLeftTriggerNum = 7;
		public static final int kRightTriggerNum = 8;
		public static final int kBackButtonNum = 9;
		public static final int kStartButtonNum = 10;
		public static final int kLeftStickButtonNum = 11;
		public static final int kRightStickButtonNum = 12;
  }
