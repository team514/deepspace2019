/*----------------------------------------------------------------------------*/
/* Copyright (c) 2018 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot.commands.groups;

import edu.wpi.first.wpilibj.command.CommandGroup;
import frc.robot.commands.drive.Wait;
import frc.robot.commands.sonar.SquareToWall;
import frc.robot.commands.vision.CenterWithVision;
import frc.robot.subsystems.DriveUtil.RobotFace;

public class AlignHatch extends CommandGroup {
  /**
   * Add your docs here.
   */
  public AlignHatch() {
    // addSequential(new CenterWithVision());
    // addSequential(new SquareToWall(RobotFace.Hatch));
    // addSequential(new DriveToVisionTarget());
    addSequential(new CenterWithVision());
    addSequential(new Wait(1.0));
    addSequential(new SquareToWall(RobotFace.Hatch));
    addSequential(new Wait(1.0));
    addSequential(new CenterWithVision());
    addParallel(new SquareToWall(RobotFace.Hatch));
  }
}
