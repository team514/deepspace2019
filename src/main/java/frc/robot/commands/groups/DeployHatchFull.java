/*----------------------------------------------------------------------------*/
/* Copyright (c) 2018 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot.commands.groups;

import edu.wpi.first.wpilibj.command.CommandGroup;
import frc.robot.commands.gyro.ResetGyro;
import frc.robot.commands.hatch.BackoutFromHatch;
import frc.robot.commands.hatch.HatchOut;
import frc.robot.commands.hatch.ReleasePanel;
import frc.robot.commands.sonar.SquareToWall;
import frc.robot.commands.vision.CenterWithVision;
import frc.robot.commands.vision.DriveToVisionTarget;
import frc.robot.commands.vision.ToggleLights;
import frc.robot.subsystems.DriveUtil.RobotFace;

public class DeployHatchFull extends CommandGroup {
  /**
   * Add your docs here.
   */
  public DeployHatchFull() {
    //Base Sequence for all "assist modes"
    addSequential(new SquareToWall(RobotFace.Hatch));
    addSequential(new ResetGyro());

    //Part 1
    addSequential(new HatchOut());
    addSequential(new ToggleLights(true));
    addSequential(new CenterWithVision());
    addSequential(new DriveToVisionTarget());
    addSequential(new ToggleLights(false));

    //Part 2
    addSequential(new ReleasePanel());
    addSequential(new BackoutFromHatch());
    // Add Commands here:
    // e.g. addSequential(new Command1());
    // addSequential(new Command2());
    // these will run in order.

    // To run multiple commands at the same time,
    // use addParallel()
    // e.g. addParallel(new Command1());
    // addSequential(new Command2());
    // Command1 and Command2 will run in parallel.

    // A command group will require all of the subsystems that each member
    // would require.
    // e.g. if Command1 requires chassis, and Command2 requires arm,
    // a CommandGroup containing them would require both the chassis and the
    // arm.
  }
}
