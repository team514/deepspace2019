/*----------------------------------------------------------------------------*/
/* Copyright (c) 2018 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot.commands.groups;

import edu.wpi.first.wpilibj.command.CommandGroup;
import frc.robot.RobotMap;
import frc.robot.commands.cargo.CargoShot;
import frc.robot.commands.drive.Wait;

public class DeployCargoRocketLow extends CommandGroup {
  /**
   * Add your docs here.
   */
  public DeployCargoRocketLow() {
    // addSequential(new DriveSonarDistance(RobotFace.Cargo, 0.2, 19));
    addParallel(new CargoShot(RobotMap.lowerRocketLowShot, RobotMap.upperRocketLowShot));
    addSequential(new Wait(0.2));
  }
}
