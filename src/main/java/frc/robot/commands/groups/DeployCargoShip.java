/*----------------------------------------------------------------------------*/
/* Copyright (c) 2018 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot.commands.groups;

import edu.wpi.first.wpilibj.command.CommandGroup;
import frc.robot.RobotMap;
import frc.robot.commands.banner.CenterOnLine;
import frc.robot.commands.cargo.CargoMotorOff;
import frc.robot.commands.cargo.CargoShot;
import frc.robot.commands.drive.DriveEncoder;
import frc.robot.commands.drive.SetDriveDirection;
import frc.robot.commands.drive.Wait;
import frc.robot.commands.gyro.ResetGyro;
import frc.robot.subsystems.DriveUtil;

public class DeployCargoShip extends CommandGroup {
  /**
   * Add your docs here.
   */
  public DeployCargoShip() {
    addSequential(new SetDriveDirection(DriveUtil.RobotFace.Cargo));
    addSequential(new CenterOnLine());
    addSequential(new ResetGyro());
    // addSequential(new DriveSonarDistance(RobotFace.Cargo, 0.2, 19));
    addSequential(new DriveEncoder(RobotMap.deployCargoShip, RobotMap.encoderDriveSpeed));
    // addSequential(new Wait(0.2));
    addParallel(new CargoShot(RobotMap.upperRocketMidShot, RobotMap.lowerRocketMidShot));
    addSequential(new Wait(0.2));
    addSequential(new CargoMotorOff());
  }
}
