/*----------------------------------------------------------------------------*/
/* Copyright (c) 2018 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot.commands.hatch;

import edu.wpi.first.wpilibj.command.Command;
import frc.robot.Robot;
import frc.robot.RobotMap;

public class BackoutFromHatch extends Command {
  boolean done = false;

  public BackoutFromHatch() {
    requires(Robot.driveUtil);
  }

  // Called just before this Command runs the first time
  @Override
  protected void initialize() {
    done = false;
  }

  // Called repeatedly when this Command is scheduled to run
  @Override
  protected void execute() {
    Robot.driveUtil.driveMecanum(0.0, -RobotMap.driveMecanumY, 0.0, true);
    if((Robot.driveUtil.getDistanceHatchLeft() >= RobotMap.deployHatchBackupDistance) || 
      (Robot.driveUtil.getDistanceHatchRight() >= RobotMap.deployHatchBackupDistance)){
      Robot.driveUtil.driveMecanum(0.0, 0.0, 0.0, false);
      done = true;
    }
  }

  // Make this return true when this Command no longer needs to run execute()
  @Override
  protected boolean isFinished() {
    return done;
  }

  // Called once after isFinished returns true
  @Override
  protected void end() {
  }

  // Called when another command which requires one or more of the same
  // subsystems is scheduled to run
  @Override
  protected void interrupted() {
  }
}
