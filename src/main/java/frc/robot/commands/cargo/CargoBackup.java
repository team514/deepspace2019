/*----------------------------------------------------------------------------*/
/* Copyright (c) 2018 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot.commands.cargo;

import edu.wpi.first.wpilibj.command.Command;
import frc.robot.Robot;
import frc.robot.RobotMap;

public class CargoBackup extends Command {
  boolean done;
  double time, endTime, givenTime;
  public CargoBackup(double d) {
    // Use requires() here to declare subsystem dependencies
    // eg. requires(chassis);
    requires(Robot.driveUtil);
    time = d;
  }

  // Called just before this Command runs the first time
  @Override
  protected void initialize() {
    endTime = System.currentTimeMillis() + (time*1000);
    done = false;
  }

  // Called repeatedly when this Command is scheduled to run
  @Override
  protected void execute() {
    if(System.currentTimeMillis() >= endTime){
      Robot.driveUtil.driveMecanum(0, 0, 0, false);
      done = true;
    }else{
      Robot.driveUtil.driveMecanum(0.0, RobotMap.driveMecanumY, 0.0, true);
    }
    // Robot.driveUtil.driveMecanum(0.0, RobotMap.driveMecanumY, 0.0, true);
    // if((Robot.driveUtil.getDistanceCargoLeft() >= distance) || 
    //   (Robot.driveUtil.getDistanceCargoRight() >= distance)){
    //   Robot.driveUtil.driveMecanum(0.0, 0.0, 0.0, true);
    //   done = true;
    // }
  }

  // Make this return true when this Command no longer needs to run execute()
  @Override
  protected boolean isFinished() {
    return done;
  }

  // Called once after isFinished returns true
  @Override
  protected void end() {
  }

  // Called when another command which requires one or more of the same
  // subsystems is scheduled to run
  @Override
  protected void interrupted() {
  }
}
