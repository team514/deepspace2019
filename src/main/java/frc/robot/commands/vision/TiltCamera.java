/*----------------------------------------------------------------------------*/
/* Copyright (c) 2018 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot.commands.vision;

import edu.wpi.first.wpilibj.command.Command;
import frc.robot.Robot;
import frc.robot.RobotMap;

public class TiltCamera extends Command {
  double servoAngle;

  public TiltCamera() {
    // Use requires() here to declare subsystem dependencies
    // eg. requires(chassis);
    requires(Robot.visionUtil);
  }

  // Called just before this Command runs the first time
  @Override
  protected void initialize() {
    Robot.visionUtil.setServoAngle(RobotMap.camServoDefaultAngle);
    servoAngle = Robot.visionUtil.getServoAngle();
  }

  // Called repeatedly when this Command is scheduled to run
  @Override
  protected void execute() {
    if(Robot.oi.getLeftThumbstick() >= 0.5){
      servoAngle = servoAngle + -1.0;
    }
    if(Robot.oi.getLeftThumbstick() <= -0.5){
      servoAngle = servoAngle + 1.0;
    }
    servoAngle = Math.max(Math.min(servoAngle, RobotMap.camServoMax), RobotMap.camServoMin);
    Robot.visionUtil.setServoAngle(servoAngle);
  }

  // Make this return true when this Command no longer needs to run execute()
  @Override
  protected boolean isFinished() {
    return false;
  }

  // Called once after isFinished returns true
  @Override
  protected void end() {
  }

  // Called when another command which requires one or more of the same
  // subsystems is scheduled to run
  @Override
  protected void interrupted() {
  }
}
