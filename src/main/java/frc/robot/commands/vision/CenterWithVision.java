/*----------------------------------------------------------------------------*/
/* Copyright (c) 2018 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot.commands.vision;

import edu.wpi.first.wpilibj.command.Command;
import frc.robot.Robot;
import frc.robot.RobotMap;

public class CenterWithVision extends Command {
  boolean done;
  
  public CenterWithVision() {
    requires(Robot.driveUtil);
  }

  // Called just before this Command runs the first time
  @Override
  protected void initialize() {
    Robot.driveUtil.resetGyro();
    done = false;
  }

  // Called repeatedly when this Command is scheduled to run
  @Override
  protected void execute() {
    if((Robot.visionUtil.getHorizontalOffset() >= -RobotMap.centerWindowDegrees) && 
      (Robot.visionUtil.getHorizontalOffset() <= RobotMap.centerWindowDegrees)){
      Robot.driveUtil.driveMecanum(0.0, 0.0, 0.0, false);
      done = true;
    }else{
      if(Robot.visionUtil.getHorizontalOffset() < -(RobotMap.centerWindowDegrees - 1)){
        Robot.driveUtil.driveMecanum(RobotMap.driveMecanumXVision, 0.0, 0.0, true);
      }
      if(Robot.visionUtil.getHorizontalOffset() > (RobotMap.centerWindowDegrees - 1)){
        Robot.driveUtil.driveMecanum(-RobotMap.driveMecanumXVision, 0.0, 0.0, true);
      }
    }
  }

  // Make this return true when this Command no longer needs to run execute()
  @Override
  protected boolean isFinished() {
    return done;
  }

  // Called once after isFinished returns true
  @Override
  protected void end() {
  }

  // Called when another command which requires one or more of the same
  // subsystems is scheduled to run
  @Override
  protected void interrupted() {
  }
}
