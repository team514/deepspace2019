/*----------------------------------------------------------------------------*/
/* Copyright (c) 2018 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot.commands.drive;

import edu.wpi.first.wpilibj.command.Command;
import frc.robot.Robot;
import frc.robot.RobotMap;
import frc.robot.subsystems.DriveUtil.RobotFace;

public class CenterAndSquare extends Command {
  boolean done = false;
  double storedAngle = 1000;
  boolean left = false;
  RobotFace RobotFace;
  double leftSonar, rightSonar, gyroValue;
  int step = 0;

  public CenterAndSquare(RobotFace sm) {
    // Use requires() here to declare subsystem dependencies
    // eg. requires(chassis);
    requires(Robot.driveUtil);
    RobotFace = sm;
  }
  // Called just before this Command runs the first time
  @Override
  protected void initialize() {
    done = false;
    storedAngle = 1000;
    left = false;
    step = 0;
  }

  // Called repeatedly when this Command is scheduled to run
  @Override
  protected void execute() {
    switch(this.RobotFace){
      case Hatch:{
        leftSonar = Robot.driveUtil.getDistanceHatchLeft();
        rightSonar = Robot.driveUtil.getDistanceHatchRight();
        break;
      }
      case Cargo:{
        leftSonar = Robot.driveUtil.getDistanceCargoLeft();
        rightSonar = Robot.driveUtil.getDistanceCargoRight();
        break;
      }
      default:
        break;
    }

    switch(step){
      case 1:{
        if (storedAngle == 1000) {
          Robot.driveUtil.resetGyro();
    
          double x = leftSonar;
          double y = rightSonar;
    
          double angle = Math.toDegrees(Math.atan(RobotMap.S2W_SonarDistance / (y - x)));
          double angleRounded = ((int) (angle * 10000)) / 10000.0;
    
          left = x < y;
    
          if (left) {
            storedAngle = 90 - angleRounded;
          } else {
            storedAngle = angleRounded + 90;
          }
        }
        gyroValue = Robot.driveUtil.getGyro();
        if (left) {
          if (Math.abs(gyroValue) < storedAngle) {
            Robot.driveUtil.driveMecanum(0, 0, -RobotMap.driveMecanumZ, false);
          } else {
            Robot.driveUtil.driveMecanum(0, 0, 0, false);
            step++;
          }
        } else {
          if (Math.abs(gyroValue) < storedAngle) {
            Robot.driveUtil.driveMecanum(0, 0, RobotMap.driveMecanumZ, false);
          } else {
            Robot.driveUtil.driveMecanum(0, 0, 0, false);
            step++;
          }
        }
        break;
      }

      case 0:{
          if((Robot.visionUtil.getHorizontalOffset() >= -RobotMap.centerWindowDegrees) && 
          (Robot.visionUtil.getHorizontalOffset() <= RobotMap.centerWindowDegrees)){
          Robot.driveUtil.driveMecanum(0.0, 0.0, 0.0, false);
          step++;
        }else{
          if(Robot.visionUtil.getHorizontalOffset() < -(RobotMap.centerWindowDegrees - 1)){
            Robot.driveUtil.driveMecanum(RobotMap.driveMecanumXVision, 0.0, 0.0, true);
          }
          if(Robot.visionUtil.getHorizontalOffset() > (RobotMap.centerWindowDegrees - 1)){
            Robot.driveUtil.driveMecanum(-RobotMap.driveMecanumXVision, 0.0, 0.0, true);
          }
        }

        break;
      }
      case 2:{
        done = true;
        break;
      }
      default:
        break;
    }

    

  }

  // Make this return true when this Command no longer needs to run execute()
  @Override
  protected boolean isFinished() {
    return done;
  }

  // Called once after isFinished returns true
  @Override
  protected void end() {
  }

  // Called when another command which requires one or more of the same
  // subsystems is scheduled to run
  @Override
  protected void interrupted() {
  }
}
