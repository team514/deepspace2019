/*----------------------------------------------------------------------------*/
/* Copyright (c) 2018 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot.commands.drive;

import edu.wpi.first.wpilibj.command.Command;
import frc.robot.Robot;
import frc.robot.subsystems.DriveUtil.RobotFace;

public class DriveSonarDistance extends Command {
  RobotFace face;
  double distance, 
  ySpeed;
  boolean done = false;

  public DriveSonarDistance(RobotFace face, double ySpeed, double distance) {
    // Use requires() here to declare subsystem dependencies
    // eg. requires(chassis);
    requires(Robot.driveUtil);
    this.face = face;
    this.ySpeed = ySpeed;
    this.distance = distance;
  }

  // Called just before this Command runs the first time
  @Override
  protected void initialize() {
    done = false;
  }

  // Called repeatedly when this Command is scheduled to run
  @Override
  protected void execute() {
    double sonarDistance;
    switch (face) {
      case Hatch:{
        sonarDistance = Robot.driveUtil.getDistanceHatchRight();
        break;
      }
      case Cargo:{
        sonarDistance = Robot.driveUtil.getDistanceCargoRight();
        break;
      }
      default:{
        sonarDistance = 0;
        break;
      }
    }
    if(sonarDistance>=distance){
      Robot.driveUtil.driveMecanum(0, 0, 0, false);
      done = true;
      return;
    }
    Robot.driveUtil.driveMecanum(0, ySpeed, 0, true);
  }

  // Make this return true when this Command no longer needs to run execute()
  @Override
  protected boolean isFinished() {
    return done;
  }

  // Called once after isFinished returns true
  @Override
  protected void end() {
  }

  // Called when another command which requires one or more of the same
  // subsystems is scheduled to run
  @Override
  protected void interrupted() {
  }
}
