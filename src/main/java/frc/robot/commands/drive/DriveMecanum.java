/*----------------------------------------------------------------------------*/
/* Copyright (c) 2018 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot.commands.drive;

import edu.wpi.first.wpilibj.command.Command;
import frc.robot.Robot;
import frc.robot.RobotMap;

public class DriveMecanum extends Command {

  boolean zPress;

  public DriveMecanum() {
    requires(Robot.driveUtil);
  }

  // Called just before this Command runs the first time
  @Override
  protected void initialize() {
  }

  // Called repeatedly when this Command is scheduled to run
  @Override
  protected void execute() {
    zPress = Robot.oi.isZpressed();
    if (Robot.driveUtil.getReverseDrive()){
      if(Robot.driveUtil.getSplitStick()){
        Robot.driveUtil.driveMecanum(-Robot.oi.getRightX(),
                                     -Robot.oi.getRightY(),
                                     (Robot.oi.getRightZ() * RobotMap.zAdjust),
                                     false,true, zPress);
      }else{
        Robot.driveUtil.driveMecanum(-Robot.oi.getRightX(),
                                     -Robot.oi.getRightY(),
                                     (Robot.oi.getLeftZ() * RobotMap.zAdjust),
                                     false,true, zPress);
      } 
    }else{
      if(Robot.driveUtil.getSplitStick()){
        Robot.driveUtil.driveMecanum(Robot.oi.getRightX(),
                                      Robot.oi.getRightY(),
                                      (Robot.oi.getRightZ() * RobotMap.zAdjust),
                                      false,true, zPress);
      }else{
        Robot.driveUtil.driveMecanum(Robot.oi.getRightX(),
                                      Robot.oi.getRightY(),
                                      (Robot.oi.getLeftZ() * RobotMap.zAdjust),
                                      false,true, zPress);
      } 
    }
  }

  // Make this return true when this Command no longer needs to run execute()
  @Override
  protected boolean isFinished() {
    return false;
  }

  // Called once after isFinished returns true
  @Override
  protected void end() {
  }

  // Called when another command which requires one or more of the same
  // subsystems is scheduled to run
  @Override
  protected void interrupted() {
  }
}
