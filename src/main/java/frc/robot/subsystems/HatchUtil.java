/*----------------------------------------------------------------------------*/
/* Copyright (c) 2018 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot.subsystems;

import edu.wpi.first.wpilibj.Solenoid;
import edu.wpi.first.wpilibj.command.Subsystem;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import frc.robot.RobotMap;
import frc.robot.commands.hatch.PivotHatch;

/**
 * Add your docs here.
 */
public class HatchUtil extends Subsystem {
  Solenoid panelLatch, hatchSwivel;

  public HatchUtil() {
    panelLatch = new Solenoid(RobotMap.panelLatch);
    hatchSwivel = new Solenoid(RobotMap.hatchSwivel);
  }

  /**
   * Releases grasp on hatch panel
   */
  public void grabPanel() {
    panelLatch.set(true);
  }

  /**
   * Secures grasp on hatch panel
   */
  public void releasePanel() {
    panelLatch.set(false);
  }

  /**
   * Move hatch panel mechanism out of frame perimeter
   */
  public void swivelOut() {
    hatchSwivel.set(true);
  }
  
  public boolean getGrabbed() {
    return panelLatch.get();
  }

  /**
   * Move hatch panel mechanism within frame perimeter
   */
  public void swivelIn() {
    hatchSwivel.set(false);
  }

  public void togglePanel() {
    panelLatch.set(!panelLatch.get());

  }

  public void updateStatus() {
    SmartDashboard.putString("Hatch Command", getCurrentCommandName());
    SmartDashboard.putBoolean("Hatch Grabbed", getGrabbed());
  }

  @Override
  public void initDefaultCommand() {
    setDefaultCommand(new PivotHatch());
  }
}
