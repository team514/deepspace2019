/*----------------------------------------------------------------------------*/
/* Copyright (c) 2018 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot.subsystems;

import com.ctre.phoenix.motorcontrol.can.WPI_VictorSPX;

import edu.wpi.first.wpilibj.ADXRS450_Gyro;
import edu.wpi.first.wpilibj.AnalogInput;
import edu.wpi.first.wpilibj.DigitalInput;
import edu.wpi.first.wpilibj.DigitalOutput;
import edu.wpi.first.wpilibj.Encoder;
import edu.wpi.first.wpilibj.SPI.Port;
import edu.wpi.first.wpilibj.SpeedController;
import edu.wpi.first.wpilibj.command.Subsystem;
import edu.wpi.first.wpilibj.drive.MecanumDrive;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import frc.robot.RobotMap;
import frc.robot.commands.drive.DriveMecanum;

/**
 * Add your docs here.
 */
public class DriveUtil extends Subsystem {
  SpeedController leftFront, leftRear, rightFront, rightRear;
  MecanumDrive drive;
  ADXRS450_Gyro gyro;
  DigitalInput leftOuter, leftInner, center, rightInner, rightOuter; //banner sensors
  
  Encoder encoder;

  AnalogInput sonarCargoLeft, sonarCargoRight, sonarHatchLeft, sonarHatchRight;
  DigitalOutput sonarCargoSequenceOutput, sonarHatchSequenceOutput;

  boolean splitStick = false;
  boolean reverseDrive = false;
  boolean leftBannersOn = false;
  boolean rightBannersOn = false;


  public DriveUtil(){
    leftFront = new WPI_VictorSPX(RobotMap.leftFrontSC);
    leftRear = new WPI_VictorSPX(RobotMap.leftRearSC);
    rightFront = new WPI_VictorSPX(RobotMap.rightFrontSC);
    rightRear = new WPI_VictorSPX(RobotMap.rightRearSC);

    leftFront.setInverted(true);
    rightFront.setInverted(true);
    leftRear.setInverted(true);
    rightRear.setInverted(true);

    drive = new MecanumDrive(leftFront, leftRear, rightFront, rightRear);
    drive.setSafetyEnabled(true);
    
    gyro = new ADXRS450_Gyro(Port.kOnboardCS0);

    encoder = new Encoder(RobotMap.driveEncoderFoward, RobotMap.driveEncoderReverse);

    sonarCargoLeft = new AnalogInput(RobotMap.sonarCargoLeft);
    sonarCargoLeft.setAverageBits(RobotMap.cargoAverageBits);
    sonarCargoLeft.setOversampleBits(RobotMap.cargoOversampleBits);

    sonarCargoRight = new AnalogInput(RobotMap.sonarCargoRight);
    sonarCargoRight.setAverageBits(RobotMap.hatchAverageBits);
    sonarCargoRight.setOversampleBits(RobotMap.hatchOversampleBits);

    sonarHatchLeft = new AnalogInput(RobotMap.sonarHatchLeft);
    sonarHatchLeft.setAverageBits(RobotMap.hatchAverageBits);
    sonarHatchLeft.setOversampleBits(RobotMap.hatchOversampleBits);

    sonarHatchRight = new AnalogInput(RobotMap.sonarHatchRight);
    sonarHatchRight.setAverageBits(RobotMap.hatchAverageBits);
    sonarHatchRight.setOversampleBits(RobotMap.hatchOversampleBits);

    // sonarHatchSequenceOutput = new DigitalOutput(RobotMap.sonarHatchSequenceOutput);
    // sonarCargoSequenceOutput = new DigitalOutput(RobotMap.sonarCargoSequenceOutput);

    leftOuter = new DigitalInput(RobotMap.leftOuter);
    leftInner = new DigitalInput(RobotMap.leftInner);
    center = new DigitalInput(RobotMap.center);
    rightInner = new DigitalInput(RobotMap.rightInner);
    rightOuter = new DigitalInput(RobotMap.rightOuter);
  }

  public enum RobotFace {
    Hatch, 
    Cargo;
  }

  public boolean getSonarRange(){
    if((getDistanceCargoLeft() <= RobotMap.sonarRange) || 
      (getDistanceCargoRight() <= RobotMap.sonarRange)){
      return true;
    }else{
      return false;
    }
  }

  public double getDistanceCargoLeft(){ 
    double av = sonarCargoLeft.getAverageVoltage();
    double mm = (1024 * av);
    double inches = (mm / 25.4);
    return inches;
  }
  
  public double getDistanceCargoRight(){
    double av = sonarCargoRight.getAverageVoltage();
    double mm = (1024 * av);
    double inches = (mm / 25.4);
    return inches;
  }

  public double getDistanceHatchLeft(){ 
    double av = sonarHatchLeft.getAverageVoltage();
    double mm = (1024 * av);
    double inches = (mm / 25.4);
    return inches;
  }
  
  public double getDistanceHatchRight(){
    double av = sonarHatchRight.getAverageVoltage();
    double mm = (1024 * av);
    double inches = (mm / 25.4);
    return inches;
  }

  public void setCargoSonarSequenceOutput(boolean b){
    sonarCargoSequenceOutput.set(b);
  }

  public boolean getCargoSonarSequenceOutput(){
    return sonarCargoSequenceOutput.get();
  }
  
  public void setHatchSonarSequenceOutput(boolean b){
    sonarHatchSequenceOutput.set(b);
  }

  public boolean getHatchSonarSequenceOutput(){
    return sonarHatchSequenceOutput.get();
  }

  public void resetEncoder(){
    encoder.reset();
  }

  public double getEncoder() {
		return Math.abs(encoder.get());
	}
  /**
   * @return Banner value for DIO port 0
   */
  public boolean getLeftOuter(){
    return leftOuter.get();
  }

  /**
   * @return Banner value for DIO port 1
   */
  public boolean getLeftInner(){
    return leftInner.get();
  }

  /**
   * @return Banner value for DIO port 2
   */
  public boolean getCenter(){
    return center.get();
  }

  /**
   * @return Banner value for DIO port 3
   */
  public boolean getRightInner(){
    return rightInner.get();
  }

  /**
   * @return Banner value for DIO port 4
   */
  public boolean getRightOuter(){
    return rightOuter.get();
  }

  public void setSplitStick(boolean s){
    this.splitStick = s;
  }

  public boolean getSplitStick(){
    return this.splitStick;
  }

  public void setReverseDrive(boolean r){
    this.reverseDrive = r;
  }

  public boolean getReverseDrive(){
    return this.reverseDrive;
  }

  public void resetGyro(){
    gyro.reset();
  }
  
  public void calibrateGyro(){
    gyro.calibrate();
  }

  public double getGyro(){
    return gyro.getAngle();
  }

  public void checkBanners(){
    if (getLeftOuter() || getLeftInner()){
      leftBannersOn = true; // 0 is on left
      rightBannersOn = false;
    }
    if (getRightOuter() || getRightInner()){
      rightBannersOn = true; //1 is on right
      leftBannersOn = false;
    }
    if(getCenter()){
      rightBannersOn = false;
      leftBannersOn = false;
    }
  }
  
  public boolean getLeftBanners(){
    return leftBannersOn;
  }

  public boolean getRightBanners(){
    return rightBannersOn;
  }

  public void driveMecanum(double x, double y, double z, boolean useGyro) {
    driveMecanum(x, y, z, useGyro, false, true);
  }

  /**
   * Operate mecanum drive train
   * 
   * @param x The x-value of the joystick (left negative, right positive)
   * @param y The y-value of the joystick (forward positive, backward negative)
   * @param z The z-value of the joystick (clockwise positive, counterclockwise negative)
   * @param useGyro Whether to drive straight with the gyro
   */
  public void driveMecanum(double x, double y, double z, boolean useGyro, boolean squaredInputs, boolean zPressed){
    if (squaredInputs) {
      x = squaredInputs(x);
      y = squaredInputs(y);

      if(zPressed){
        z = squaredInputs(z);
      }else{
        z = (z*z*z);
      }
    }
		if(useGyro){
			drive.driveCartesian(-x, y, -z, getGyro() * 2);
		}else{
      drive.driveCartesian(-x, y, -z);
		}
  }

  /**
   * Return d^2, retains sign of original value (-0.5 returns -0.25)
   * 
   * @param d A double from 0 to 1
   * @return d^2, so a d value of 0.5 returns 0.25
   */
  public double squaredInputs(double d){
    boolean negative = d<0;
    d = Math.pow(d, 2);

    if(negative){
      d*= -1;
    }
    
    return d;
  }

  /*public double SquareToWallAngle(RobotFace sm){
    double storedAngle = 1000;
    double leftSonar, rightSonar;
    boolean left;
    // done = false;
    leftSonar = 0.0;
    rightSonar = 0.0;
    switch(this.RobotFace){
      case Hatch:{
        leftSonar = getDistanceHatchLeft();
        rightSonar = getDistanceHatchRight();
        break;
      }
      case Cargo:{
        leftSonar = getDistanceCargoLeft();
        rightSonar = getDistanceCargoRight();
        break;
      }
      default:
        break;
    }

    if (storedAngle == 1000) {
      resetGyro();

      double x = leftSonar;
      double y = rightSonar;

      double angle = Math.toDegrees(Math.atan(RobotMap.S2W_SonarDistance / (y - x)));
      double angleRounded = ((int) (angle * 10000)) / 10000.0;

      left = x < y;

      if (left) {
        storedAngle = 90 - angleRounded;
      } else {
        storedAngle = angleRounded + 90;
      }
    }

    
    gyroValue = getGyro();
    if (left) {
      if (Math.abs(gyroValue) < storedAngle) {
        driveMecanum(0, 0, -RobotMap.driveMecanumZ, false);
      } else {
        driveMecanum(0, 0, 0, false);
        //done = true;
      }
    } else {
      if (Math.abs(gyroValue) < storedAngle) {
        driveMecanum(0, 0, RobotMap.driveMecanumZ, false);
      } else {
        driveMecanum(0, 0, 0, false);
        //done = true;
      }
    }
    
    return storedAngle;
  }*/
  /*public void driveMagicArc(double yAxis, double degrees, double arcAngle, RobotFace sm){
    double adj = coerce2RangeMagic(degrees, RobotMap.C2RM_inputMin, RobotMap.C2RM_inputMax);
    double angleAdjust = coerce2RangeMagicAngle(arcAngle, RobotMap.C2RMA_inputMin, RobotMap.C2RMA_inputMax);
      if((Robot.visionUtil.getHorizontalOffset() >= -RobotMap.centerWindowDegrees) && 
        (Robot.visionUtil.getHorizontalOffset() <= RobotMap.centerWindowDegrees)){
        Robot.driveUtil.driveMecanum(0.0, 0.0, 0.0, false);
      }else{
        if(Robot.visionUtil.getHorizontalOffset() < -(RobotMap.centerWindowDegrees - 1)){
          Robot.driveUtil.driveMecanum(RobotMap.driveMecanumXVision, 0.0, 0.0, true);
        }
        if(Robot.visionUtil.getHorizontalOffset() > (RobotMap.centerWindowDegrees - 1)){
          Robot.driveUtil.driveMecanum(-RobotMap.driveMecanumXVision, 0.0, 0.0, true);
        }
      }
    driveMecanum (adj, yAxis, angleAdjust, true);
  }*/
  /**
   * Drive robot based on input from vision
   * 
   * @param yAxis Positive speed in y-direction (left-right)
   * @param degrees How far the target is offcenter in degrees (left negative, right positive)
   */
  public void driveMagic(double yAxis, double degrees){
    double adj = coerce2RangeMagic(degrees, RobotMap.C2RM_inputMin, RobotMap.C2RM_inputMax);
		driveMecanum(adj, yAxis, 0.0, true);
  }

  /**
   * Get vision align drive values (all inputs are treated as angles in degrees)
   * 
   * @param input Offset from center in degrees
   * @param inputMin Minimum the input can be
   * @param inputMax Maximum the input can be
   * @return Adjustment value for drive train in x-direction to center onto target, min and max defined in RobotMap.C2RM_output(Min/Max)
   */
  public double coerce2RangeMagic(double input, double inputMin, double inputMax){
    double inputCenter;
    double outputMin, outputMax, outputCenter;
    double scale, result;
    
    //double output;
    
    // inputMin = RobotMap.C2RM_inputMin; 
    // inputMax = RobotMap.C2RM_inputMax;     
    
    outputMin = RobotMap.C2RM_outputMin;
    outputMax = RobotMap.C2RM_outputMax;
    
    /* Determine the center of the input range and output range */
    inputCenter = Math.abs(inputMax - inputMin) / 2 + inputMin;
    outputCenter = Math.abs(outputMax - outputMin) / 2 + outputMin;

    /* Scale the input range to the output range */
    scale = (outputMax - outputMin) / (inputMax - inputMin);

    /* Apply the transformation */
    result = (input + -inputCenter) * scale + outputCenter;

    /* Constrain to the output range */
    // Math.max(Math.min(result, outputMax), outputMin);

   return Math.max(Math.min(result, outputMax), outputMin);
  }

  // public double coerce2RangeMagicAngle(double input, double inputMin, double inputMax){
    // double inputCenter;
    // double outputMin, outputMax, outputCenter;
    // double scale, result;
    
    //double output;
    
    // inputMin = RobotMap.C2RM_inputMin; 
    // inputMax = RobotMap.C2RM_inputMax;     
    
    // outputMin = RobotMap.C2RMA_outputMin;
    // outputMax = RobotMap.C2RMA_outputMax;
    
    /* Determine the center of the input range and output range */
    // inputCenter = Math.abs(inputMax - inputMin) / 2 + inputMin;
    // outputCenter = Math.abs(outputMax - outputMin) / 2 + outputMin;

    /* Scale the input range to the output range */
    // scale = (outputMax - outputMin) / (inputMax - inputMin);

    /* Apply the transformation */
    // result = (input + -inputCenter) * scale + outputCenter;

    /* Constrain to the output range */
    // Math.max(Math.min(result, outputMax), outputMin);

  //  return Math.max(Math.min(result, outputMax), outputMin);
// }

  public void updateStatus(){
    SmartDashboard.putBoolean("Split Stick", splitStick);
    SmartDashboard.putBoolean("Reverse Drive", reverseDrive);
    SmartDashboard.putNumber("Cargo Left Sonar", getDistanceCargoLeft());
    SmartDashboard.putNumber("Cargo Right Sonar", getDistanceCargoRight());
    SmartDashboard.putNumber("Hatch Left Sonar", getDistanceHatchLeft());
    SmartDashboard.putNumber("Hatch Right Sonar", getDistanceHatchRight());
    SmartDashboard.putNumber("Gyro Angle", getGyro());
    SmartDashboard.putBoolean("LeftInner", getLeftInner());
    SmartDashboard.putBoolean("LeftOuter", getLeftOuter());
    SmartDashboard.putBoolean("Center", getCenter());
    SmartDashboard.putBoolean("RightInner", getRightInner());
    SmartDashboard.putBoolean("RightOuter", getRightOuter());
    SmartDashboard.putBoolean("SonarRange", getSonarRange());
    SmartDashboard.putString("Drive Command", getCurrentCommandName());
    SmartDashboard.putNumber("Encoder", getEncoder());
  }
  
  @Override
  public void initDefaultCommand() {
    setDefaultCommand(new DriveMecanum());
  }
}
