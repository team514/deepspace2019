/*----------------------------------------------------------------------------*/
/* Copyright (c) 2018 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot.subsystems;

import edu.wpi.first.networktables.NetworkTable;
import edu.wpi.first.networktables.NetworkTableEntry;
import edu.wpi.first.networktables.NetworkTableInstance;
import edu.wpi.first.wpilibj.Servo;
import edu.wpi.first.wpilibj.command.Subsystem;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import frc.robot.RobotMap;
import frc.robot.commands.vision.TiltCamera;

public class VisionUtil extends Subsystem {
  NetworkTable table;
  NetworkTableEntry ta, tv, tx, ledMode, camMode;
  double x, area, degrees;
  boolean hasTarget, lights;
  Servo msServo;

  public VisionUtil() {
    msServo = new Servo(RobotMap.msServo);

    table = NetworkTableInstance.getDefault().getTable("limelight");
    tx = table.getEntry("tx");
    tv = table.getEntry("tv");
    ta = table.getEntry("ta");
    ledMode = table.getEntry("ledMode");
    camMode = table.getEntry("camMode");
    
    lights = false;
    hasTarget = false;
    setLEDOff();

  }

  public double getCamMode(){
    return this.camMode.getDouble(0);
  }

  public double getServoAngle(){
    return msServo.getAngle();
  }

  public void setServoAngle(double a){
    msServo.setAngle(a);
  }

  public void setCamMode(double c){
    camMode.setNumber(c);
  }

  public void updateTargetData(){
    this.x = tx.getDouble(0.0);
    this.area = ta.getDouble(0.0);
    this.hasTarget = (tv.getNumber(0).doubleValue() == 1.0);
  }

  public boolean lightsOn(){
    return this.lights;
  }

  /**
   * Set limelight LEDs on or off
   * @param lights true if on, false if off
   */
  public void toggleLights(boolean lights){
    if(lights){
      setLEDOn();
      setCamMode(0);
    }else{
      setLEDOff();
      setCamMode(1);
    }
  }

  /**
   * Set limelight LEDs on
   */
  public void setLEDOn(){ 
    this.lights = true;
    ledMode.setNumber(3);
  }

  /**
   * Set limelight LEDs off
   */
  public void setLEDOff(){
    this.lights = false;
    ledMode.setNumber(1);
  }

  /**
   * Whether limelight sees a target
   * @return true if a target is visible, false if not
   */
  public boolean hasTarget(){
    return this.hasTarget;
  }

  /**
   * Get the horizontal offset value from the limelight pipeline
   * @return a value in degrees from -27 to 27 with 0 being perfectly centered, negative left and positive right
   */
  public double getHorizontalOffset(){
    return this.x;
  }
  
  /**
   * Get the area of the visible target from the limelight pipeline
   * @return a decimal from 0 to 1 representing what percentage of the limelight's field-of-view the target takes up (0% to 100%)
   */
  public double getTargetArea(){
    return this.area;
  }


  public double coerce2Range(double input){
    double inputMin, inputMax, inputCenter;
    double outputMin, outputMax, outputCenter;
    double scale, result;
    //double output;
    
    inputMin = -1.0; 
    inputMax = 1.0;
    
    outputMin = RobotMap.C2R_servoMin;
    outputMax = RobotMap.C2R_servoMax;
    
    /* Determine the center of the input range and output range */
    inputCenter = Math.abs(inputMax - inputMin) / 2 + inputMin;
    outputCenter = Math.abs(outputMax - outputMin) / 2 + outputMin;

    /* Scale the input range to the output range */
    scale = (outputMax - outputMin) / (inputMax - inputMin);
    /* Apply the transformation */
    result = (input + -inputCenter) * scale + outputCenter;
    /* Constrain to the output range */
    degrees = Math.max(Math.min(result, outputMax), outputMin);

    return degrees;
  }

  public void updateStatus() {
    SmartDashboard.putNumber("tx", this.x);
    SmartDashboard.putNumber("ta", this.area);
    SmartDashboard.putBoolean("Has Target", this.hasTarget);
    SmartDashboard.putBoolean("Lights On", lightsOn());
    SmartDashboard.putNumber("Vision Horizontal Offset", getHorizontalOffset());
    SmartDashboard.putNumber("Servo Angle", getServoAngle());
    SmartDashboard.putNumber("Cam Mode", getCamMode());
    SmartDashboard.putString("Vision Command", getCurrentCommandName());
  }

  @Override
  public void initDefaultCommand() {
    setDefaultCommand(new TiltCamera());
  }
}
