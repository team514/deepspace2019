/*----------------------------------------------------------------------------*/
/* Copyright (c) 2018 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot.subsystems;

import com.ctre.phoenix.motorcontrol.can.WPI_TalonSRX;
import edu.wpi.first.wpilibj.DigitalInput;
import edu.wpi.first.wpilibj.command.Subsystem;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import frc.robot.RobotMap;
import frc.robot.commands.cargo.CargoMotorOff;

/**
 * Add your docs here.
 */
public class CargoUtil extends Subsystem {
  WPI_TalonSRX lowerShotMotor, upperShotMotor;
  DigitalInput cargoSwitch;

  public CargoUtil(){
    lowerShotMotor = new WPI_TalonSRX(RobotMap.lowerShotMotor);
    upperShotMotor = new WPI_TalonSRX(RobotMap.upperShotMotor);
    cargoSwitch = new DigitalInput(RobotMap.cargoSwitch);
  }

  public boolean getCargoSwitch(){
    return cargoSwitch.get();
  }

  public void setLowerMotorSpeed(double speed){
    lowerShotMotor.set(speed);
  }

  public void setUpperMotorSpeed(double speed){
    upperShotMotor.set(speed);
  }

  public void updateStatus(){
    SmartDashboard.putBoolean("Cargo Switch", getCargoSwitch());
    SmartDashboard.putString("Cargo Command", getCurrentCommandName());
  }

  @Override
  public void initDefaultCommand() {
    setDefaultCommand(new CargoMotorOff());
  }
}
