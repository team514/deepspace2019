/*----------------------------------------------------------------------------*/
/* Copyright (c) 2017-2018 FIRST. All Rights Reserved.                        */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot;

import edu.wpi.first.wpilibj.Joystick;
import edu.wpi.first.wpilibj.buttons.JoystickButton;
import frc.robot.commands.banner.CenterOnLine;
import frc.robot.commands.cargo.CargoIn;
import frc.robot.commands.cargo.CargoOut;
import frc.robot.commands.cargo.CargoShot;
import frc.robot.commands.drive.ResetEncoder;
import frc.robot.commands.drive.ToggleDriveDirection;
import frc.robot.commands.drive.ToggleDriverMode;
import frc.robot.commands.groups.CenterWithVisionGroup;
import frc.robot.commands.groups.DeployCargoRocketHigh;
import frc.robot.commands.groups.DeployCargoRocketLow;
import frc.robot.commands.groups.DeployCargoRocketMid;
import frc.robot.commands.groups.DeployCargoShip;
import frc.robot.commands.hatch.PanelToggle;
import frc.robot.commands.vision.ToggleLights;

/**
 * This class is the glue that binds the controls on the physical operator
 * interface to the commands and command groups that allow control of the robot.
 */
public class OI {
  Joystick left, right, controller;
  JoystickButton splitStick, limeLights, driveReverse, panelToggle, cargoIn, cargoOut,
   rocketMidShot, deployCargoShip, rocketLowShot, centerWithVisionGroup, centerOnLine,
   rocketHighShot, cargoManualShot, resetEncoder;

    public OI(){
      //Joysticks
      left = new Joystick(0);
      right = new Joystick(1);
      controller = new Joystick(2);

      //Buttons
        //Right Stick
          resetEncoder = new JoystickButton(right,11);

        //Left Stick
          driveReverse = new JoystickButton(left, RobotMap.kJoystickButton6);
          splitStick = new JoystickButton(left, RobotMap.kJoystickButton5);
  
        //Controller
          limeLights = new JoystickButton(controller, RobotMap.kLeftStickButtonNum);
          cargoIn = new JoystickButton(controller, RobotMap.kLeftTriggerNum);
          cargoOut = new JoystickButton(controller, RobotMap.kRightTriggerNum);
          panelToggle = new JoystickButton(controller, RobotMap.kLeftBumperNum); 
          rocketLowShot = new JoystickButton(controller, RobotMap.kAButtonNum);
          rocketMidShot = new JoystickButton(controller, RobotMap.kBButtonNum);
          centerWithVisionGroup = new JoystickButton(controller, RobotMap.kStartButtonNum);
          centerOnLine = new JoystickButton(controller, RobotMap.kBackButtonNum);
          deployCargoShip = new JoystickButton(controller, RobotMap.kXButtonNum);
          rocketHighShot = new JoystickButton(controller, RobotMap.kYButtonNum);
          cargoManualShot = new JoystickButton(controller, RobotMap.kRightBumperNum);
          

      //Button Actions
        resetEncoder.whenPressed(new ResetEncoder());
        deployCargoShip.whenPressed(new DeployCargoShip());
        centerOnLine.whenPressed(new CenterOnLine());
        limeLights.whenPressed(new ToggleLights());
        splitStick.whenPressed(new ToggleDriverMode());     
        driveReverse.whenPressed(new ToggleDriveDirection());
        cargoIn.whileHeld(new CargoIn());
        cargoOut.whileHeld(new CargoOut());
        panelToggle.whenPressed(new PanelToggle());
        rocketLowShot.whileHeld(new DeployCargoRocketLow());
        rocketMidShot.whileHeld(new DeployCargoRocketMid());
        rocketHighShot.whileHeld(new DeployCargoRocketHigh());
        centerWithVisionGroup.whenPressed(new CenterWithVisionGroup());
        cargoManualShot.whileHeld(new CargoShot(RobotMap.upperDefaultShotSpeed, RobotMap.lowerDefaultShotSpeed));
    }

    public double getRightX(){
      return right.getX();
    }

    public double getRightY(){
      return right.getY();
    }

    public double getRightZ(){
      return right.getZ();
    }

    public double getLeftZ(){
      return left.getZ();
    }

    public int getPOV(){
      return controller.getPOV();
    }

    public double getLeftThumbstick(){
      return -controller.getRawAxis(1);
    }

    public boolean isZpressed(){
      return left.getRawButton(RobotMap.kJoystickButton4);
    }
}
